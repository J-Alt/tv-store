// Styles
import './App.css';
// Components
import MainNav from './Components/Navbar/MainNav';
import Banner from './Components/Banner/Banner';
import MainLayout from './Components/Content/MainLayout';
import Footer from './Components/Footer/Footer';

function App() {
  return (
    <div className="App">
      <MainNav />
      <Banner />
      <MainLayout />
      <Footer />
    </div>
  );
}

export default App;
