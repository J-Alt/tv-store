// Styles
import '../../styles/navbar.css';
// Packages Imports
//Bootstrap
import { Navbar, Nav, InputGroup, FormControl } from 'react-bootstrap';
//React dropdown
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

function MainNav() {
  const options = ["FR", "ENG"]
  return (
    <Navbar bg="bg-white" expand="lg" className="p-0 my-2">
      <Navbar.Brand href="/" className="logo">Logo Here</Navbar.Brand>
      <div className='w-100'>
        <Nav className="me-auto">
          <InputGroup className="search-bar">
            <FormControl
              placeholder="Recipient's username"
              aria-label="Recipient's username"
              aria-describedby="basic-addon2"
            />
            <InputGroup.Text id="basic-addon2"><i className="icon icon-search"></i></InputGroup.Text>
          </InputGroup>
          <div className="menu-items mt-1">
            <div className="d-flex ml-auto">
              <Dropdown className='ddLanguages mx-2' options={options} value={"ENG"} />
              <Nav.Link href="/" className="mx-2"><i className="icon icon-menu-bag"></i></Nav.Link>
              <Nav.Link href="/" className="mx-2"><i className="icon icon-profile"></i></Nav.Link>
            </div>
          </div>
        </Nav>
      </div>
    </Navbar>
  );
}

export default MainNav;