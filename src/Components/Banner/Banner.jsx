//Styles
import '../../styles/banner.css'

function Banner() {
    return (
        <div className="banner">
            <img src={require('../../images/banner.png')} alt="banner"></img>
        </div>
    );
}

export default Banner;