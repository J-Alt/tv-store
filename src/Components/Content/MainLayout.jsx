//Styles
import '../../styles/mainlayout.css'
// Bootstrap imports
import { Row, Col } from "react-bootstrap";
// Components
import ProductsLayout from "./Products/ProductsLayuot";
import Filters from './Filters/Filters';


function MainLayout() {
    return (
        <Row className="m-0 main-layout">
            <Col md={3} className='py-3'>
                <Filters />
            </Col>
            <Col md={9}>
                <ProductsLayout />
            </Col>
        </Row>
    )
};
export default MainLayout;