//Styles
import "../../../styles/filters.css"
//React imports
import { useState } from "react";
// Bootstrap imports
import { Accordion, Button, Form, Row, Col } from "react-bootstrap";
//Components
import MultiRangeSlider from "./MultiRangeSlider";

function Filters() {
  const brands = ["All", "LG", "Loewe", "Panasonic", "Philips", "Salora", "Samsung", "Sharp", "Sony", "Tcl", "Thomson"];
  const availableColors = ["black", "red", "blue", "green", "yellow", "orange", "gray", "white"];
  const selectedColors = ["black", "red", "blue"]
  const [colors, setColors] = useState(null);


  return (
    <div className="filters">
      <div className="filters-header d-flex p-3">
        <h4>FILTER BY:</h4>
        <Button variant="outline-light">Reset</Button>
      </div>
      <Accordion>
        <Accordion.Item eventKey="0">
          <Accordion.Header>Price</Accordion.Header>
          <Accordion.Body>
            <p>Price Range Selected</p>
            <p className="price-range">
              <span>€ </span>
              <span>300 </span>
              <span>- </span>
              <span>€ </span>
              <span>1.500</span>
            </p>
            <MultiRangeSlider
              min={150}
              max={1500}
              onChange={({ min, max }) => console.log(`min = ${min}, max = ${max}`)}
            />
          </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="1">
          <Accordion.Header>Brand</Accordion.Header>
          <Accordion.Body>
            <Form>
              {brands.map((el, i) =>
                <Form.Check
                  type="checkbox"
                  id={i}
                  key={i}
                  label={el}
                  className="mb-3"
                />
              )}

            </Form>
          </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="2">
          <Accordion.Header>Product color</Accordion.Header>
          <Accordion.Body>
            <Row className="flex-wrap">
              {availableColors.map((el, i) =>
                <Col xs={2} md={3} key={i} className="colors circles p-0">
                  <Form.Check
                    type="checkbox"
                    id={el}
                    label=""
                    className="mb-3 p-1"
                  />
                </Col>
              )}
            </Row>
            <Row>
              <Col>
                <p className="mb-2">Selected colors</p>
              </Col>
            </Row>
            {selectedColors.map((el, i) =>
              <Row key={i} className="mb-2">
                <Col>
                  <Button variant="outline-dark" className="mx-1 p-0 picked-colors"></Button>
                  <span> {el}</span>
                </Col>
              </Row>
            )}
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </div>
  );
}

export default Filters;