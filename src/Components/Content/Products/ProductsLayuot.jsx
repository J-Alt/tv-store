// Bootstrap imports
import { Row, Col } from "react-bootstrap";
//Components
import Product from "./Product";

function ProductsLayout() {
    const products = require("../../../data/data.json");

    return (
        <Row>
            {products.map((el, i) =>
                <Col lg={4} md={6} key={i} className="p-3">
                    <Product title={el.model} properties={el.properties} raiting={el.raiting} reviews={el.reviews} price={el.price} />
                </Col>
            )}
        </Row>
    )
};
export default ProductsLayout;