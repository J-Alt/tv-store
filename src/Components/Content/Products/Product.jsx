//Styles
import "../../../styles/product.css"
//Bootstrap imports
import { Card, Row, Col } from "react-bootstrap";

function Product(props) {


    return (
        <Card className="p-2">
            <Card.Img variant="top" src={require('../../../images/product.png')} />
            <Card.Body>
                <Card.Title>{props.title}</Card.Title>
                <Row>
                    <Col xs={9}>
                        <div className="d-flex flex-wrap">
                            <div className="raiting">
                                <i className="icon icon-star"></i>
                                <span className="mx-1"> {props.raiting} </span>
                            </div>
                            <div className="mx-1 reviews">
                                <span> ({props.reviews})</span>
                                <span> Reviews</span>
                            </div>
                        </div>
                        <ul className="properties">
                            {props.properties.map((el, i) => <li key={i}>{el}</li>)}
                        </ul>
                        <div className="price">
                            <span>€</span>
                            <span>{props.price}</span>
                        </div>
                    </Col>
                    <Col xs={3} className="actions-container d-flex justify-content-end">
                        <div className="actions mx-2">
                            <i className="d-block icon icon-compare"></i>
                            <i className="d-block icon icon-fave"></i>
                            <i className="d-block icon icon-bag"></i>
                        </div>
                    </Col>
                </Row>

            </Card.Body>
        </Card>
    )
};
export default Product;