//Styles
import '../../styles/footer.css';
// Bootstrap imports
import { Row, Col, Carousel } from "react-bootstrap";

function Footer() {
    const footerBrands = ["brand-1", "brand-2", "brand-3", "brand-4", "brand-5"];

    return (
        <Row className='footer m-0'>
            <Col lg={3} className="footer-title">
                <h4>MORE THAN 100 BRANDS</h4>
            </Col>
            <Col lg={9} className='footer-carousel'>
                <Row className='items-holder'>
                    <Col xs={1}></Col>
                    <Col xs={10}>
                        <Carousel
                            nextIcon={<div className="arrow arrow-right"></div>}
                            prevIcon={<div className="arrow arrow-left"></div>}
                            indicators={false}
                        >
                            <Carousel.Item className='wrapper-footer-brands-hold'>
                                <div className='wrapper-footer-brands d-flex'>
                                    {footerBrands.map((el, i) => {
                                        return (<div className='footer-brands' key={i}>
                                            <img src={require(`../../images/${el}.png`)} alt="brands"></img>
                                        </div>);
                                    })}
                                </div>
                            </Carousel.Item>
                            <Carousel.Item className='wrapper-footer-brands-hold'>
                                <div className='wrapper-footer-brands d-flex'>
                                    {footerBrands.map((el, i) => {
                                        return (<div className='footer-brands' key={i}>
                                            <img src={require(`../../images/${el}.png`)} alt="brands"></img>
                                        </div>);
                                    })}
                                </div>
                            </Carousel.Item>
                        </Carousel>
                    </Col>
                    <Col xs={1}></Col>
                </Row>
            </Col>
        </Row>
    );
}

export default Footer;